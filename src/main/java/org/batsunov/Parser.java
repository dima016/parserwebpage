package org.batsunov;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class Parser {

    public String returnHTMLCodeFromURL(String siteRef) {
        String htmlCode = "";

        try {
            URL url = new URL(siteRef);
            URLConnection urlConnection = url.openConnection();
            InputStream stream = urlConnection.getInputStream();
            int i;
            while ((i = stream.read()) != -1) {
                htmlCode += String.valueOf((char) i);
            }
        } catch (Exception e) {
            e.getStackTrace();
        }

        return htmlCode;
    }

}
