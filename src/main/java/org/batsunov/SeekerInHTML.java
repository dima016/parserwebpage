package org.batsunov;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SeekerInHTML {

    private String stringHTML;

    public SeekerInHTML(String stringHTML) {
        this.stringHTML = stringHTML;
    }

    public List<String> seekImg() {

        List<String> allGroupsList = new ArrayList<>();
        Pattern pattern = Pattern.compile("(<img.*?src.\"(.*?)\")");
        Matcher matcher = pattern.matcher(stringHTML);


        while (matcher.find()) {
            allGroupsList.add(matcher.group(2));
        }


        return allGroupsList;
    }
}
