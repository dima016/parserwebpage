package org.batsunov;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindDir {

    public static String findDirAndNameForFile(String str) {

        Pattern pattern = Pattern.compile(":\\/(\\/.*?)\\/()(.*)(\\/.*\\.[a-z]{3})");
        Matcher matcher = pattern.matcher(str);

        if (matcher.find()) {
            return matcher.group(1)+matcher.group(4);
        }

        return "";
    }
}
