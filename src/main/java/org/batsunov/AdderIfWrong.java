package org.batsunov;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdderIfWrong {//не лучшее название,но лучше не могу придумать для этого класса

    public String addIfWrongRefForIMG(String str, String refSite) { //метод добавляет имя сайта,если в ссылке на картинке его нет

        Pattern pattern = Pattern.compile("(https:\\/\\/)(.*?)\\/");
        Matcher matcherStr = pattern.matcher(str);
        Matcher matcherRefSite = pattern.matcher(refSite);

        if (matcherStr.find()) {
            return str;
        } else if (matcherRefSite.find()) {

            return matcherRefSite.group() + str;
        }


        return "";
    }


}
