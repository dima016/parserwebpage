package org.batsunov;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Введите ссылку на сайт для поиска и загрузки изображения:");
        String siteRef = in.nextLine();

        System.out.print("Введите директорию в какую будут скачиваться изображения(Пример формата (D:\\Картинки   Дальше будет подставлено имя сайта):");
        String pathName = in.nextLine();

        Parser parser = new Parser();
        String htmlCode = parser.returnHTMLCodeFromURL(siteRef);//возвращаем html код страницы

        SeekerInHTML parserHTML = new SeekerInHTML(htmlCode);//для поиска ссылок на картинки

        Downloader downloader = new Downloader();
        if (parserHTML.seekImg().size() > 0) {
            for (String urlIMG : parserHTML.seekImg()) {
                downloader.downloadIMGFromRef(urlIMG, siteRef, pathName);
            }

            System.out.println("Программа закончила свое выполнение,заходите в " + pathName + " и проверяйте наличие картинок в папе названия сайта");
        } else {

            System.out.println("Программе не удалось найти любые изображения");
        }

    }
}
