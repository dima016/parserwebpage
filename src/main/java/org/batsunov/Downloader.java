package org.batsunov;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;


public class Downloader {

    public void downloadIMGFromRef(String url, String site, String pathName) {

        AdderIfWrong adder = new AdderIfWrong();
        try {
            url = adder.addIfWrongRefForIMG(url, site);
            File file = new File(pathName + FindDir.findDirAndNameForFile(url));
            URL myUrl = new URL(url);
            FileUtils.copyURLToFile(myUrl, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
